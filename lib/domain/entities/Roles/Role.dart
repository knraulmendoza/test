/**
 * Woin 
 *
 * Woin TDD architecture + clean code
 * 
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app_woin_front_movil
 * @since  0.1 rev
 * @author Kenneth Raul Mendoza Lopez <kenne_8@outlook.es>
 * @file Domain/Entities
 * @observations use abstractions for Entities
 * @HU 0: Infraestructure
 * @task Task: Infraestructura
 */
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Role extends Equatable {
  final String name, description;
  Role({@required this.name, @required this.description});
  List<Object> get props => [];
}

