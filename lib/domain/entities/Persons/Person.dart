/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app_woin_front_movil
 * @since  0.1 rev
 * @author Kenneth Raul Mendoza Lopez <kenne_8@outlook.es>
 * @file Domain/Entities
 * @observations use abstractions for Entities
 * @HU 0: Infraestructure
 * @task Task: Infraestructura
 */
import 'package:app_woin_front_movil/domain/entities/Persons/ValuesObjets/Name.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Person extends Equatable{
  final Name name;
  final int gender;
  final DateTime birthDate;
  Person({@required this.name, @required this.birthDate, @required this.gender});
  @override
  List<Object> get props => [name, birthDate, gender];
}