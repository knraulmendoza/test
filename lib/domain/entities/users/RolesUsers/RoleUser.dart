/**
 * Woin 
 *
 * Woin DDD architecture
 * use of Hexagonal Programming and DDD
 *
 * Hexagonal Architecture that allows us to develop and test our application in isolation from the framework,
 * the database, third-party packages and all those elements that are around our application
 *
 * @link https://dev-woin@dev.azure.com/dev-woin/app.woin/_git/app_woin_front_movil
 * @since  0.1 rev
 * @author Kenneth Raul Mendoza Lopez <kenne_8@outlook.es>
 * @file Domain/Entities
 * @observations use abstractions for Entities
 * @HU 0: Infraestructure
 * @task Task: Infraestructura
 */
import 'package:app_woin_front_movil/domain/entities/Roles/Role.dart';
import 'package:app_woin_front_movil/domain/entities/users/user.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class RoleUser extends Equatable {
  final int roleId;
  final Role role;
  final User user;
  RoleUser({this.roleId, @required this.role, @required this.user});

  @override
  List<Object> get props => [roleId, role, user];
}